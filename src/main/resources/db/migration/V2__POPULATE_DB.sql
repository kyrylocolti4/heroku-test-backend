INSERT INTO roles (role_name)
VALUES ('USER'),
       ('ADMIN');

INSERT INTO users (email,password, is_enabled)
VALUES ('admin@gmail.com', '$2a$12$owtTS8Q5teMgBiUMju1cy.NNDMGUhEKnelNJ8uL2Q/4FsvFg7/6Yq', true);
-- password - Admin123

INSERT INTO users_roles (user_id, role_id)
VALUES (1, 2);

INSERT INTO categories (category_name_en, category_name_ua)
VALUES  ('Help to the military', 'Допомога військовим'),
('Evacuation', 'Евакуація'),
('Medicine', 'Медицина'),
('Food and water', 'Продукти харчування та вода'),
('Household goods', 'Побутові товари'),
('Clothes', 'Одяг'),
('Housing', 'Житло'),
('Transportation (vehicle)', 'Перевозка (транспортний засіб)'),
('Psychological assistance', 'Психологічна допомога'),
('Legal assistance', 'Юридична допомога'),
('Animals', 'Тварини'),
('Other', 'Інше');

