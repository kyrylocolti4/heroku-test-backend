package boot.camp.heartofukrainebootcamp.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Types;

@Data
@Entity
@Table(name = "images")
@NoArgsConstructor
@AllArgsConstructor
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image", nullable = false)
    @JdbcTypeCode(Types.BINARY)
    private byte[] image;

    @ManyToOne
    @JoinColumn(name = "help_request_id")
    private HelpRequest helpRequest;

    @OneToOne(mappedBy="image")
    private UserAdditionalDetails userAdditionalDetails;
}
