package boot.camp.heartofukrainebootcamp.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import java.util.List;
import lombok.Data;

@Entity
@Table(name = "categories")
@Data
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "category_name_en", nullable = false)
    private String categoryNameEn;

    @Column(name = "category_name_ua", nullable = false)
    private String categoryNameUa;

    @ManyToMany(mappedBy = "categories")
    private List<HelpRequest> helpRequests;
}
