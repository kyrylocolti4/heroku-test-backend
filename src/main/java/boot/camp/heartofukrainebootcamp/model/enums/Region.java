package boot.camp.heartofukrainebootcamp.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.NoSuchElementException;

@Getter
@RequiredArgsConstructor
public enum Region {
    VINNYTSIA("ВІННИЦЬКА"),
    VOLYN("ВОЛИНСЬКА"),
    DNIPROPETROVSK("ДНІПРОПЕТРОВСЬКА"),
    DONETSK("ДОНЕЦЬКА"),
    ZHYTOMYR("ЖИТОМИРСЬКА"),
    ZAKARPATTIA("ЗАКАРПАТСЬКА"),
    ZAPORIZHIA("ЗАПОРІЗЬКА"),
    IVANO_FRANKIVSK("ІВАНО-ФРАНКІВСЬКА"),
    KYIV("КІЇВСЬКА"),
    KIROVOGRAD("КІРОВОГРАДСЬКА"),
    LUHANSK("ЛУГАНСЬКА"),
    LVIV("ЛЬВІВСЬКА"),
    MYKOLAIV("МИКОЛАЇВСЬКА"),
    ODESA("ОДЕСЬКА"),
    POLTAVA("ПОЛТАВСЬКА"),
    RIVNE("РІВНЕНСЬКА"),
    SUMY("СУМСЬКА"),
    TERNOPIL("ТЕРНОПІЛЬСЬКА"),
    KHARKIV("ХАРКІВСЬКА"),
    KHERSON("ХЕРСОНСЬКА"),
    KHMELNYTSKYI("ХМЕЛЬНИЦЬКА"),
    CHERKASY("ЧЕРКАСЬКА"),
    CHERNIVTSI("ЧЕРНІВЕЦЬКА"),
    CHERNIHIV("ЧЕРНІГІВСЬКА");

    private final String uaRegion;

    private static final String REGION_IS_NOT_EXISTS = "Such region is not exists";

    public static Region getRegion(String region) {
        String updatedRegion = region.toUpperCase().strip();
        if (!Arrays.stream(Region.values())
                .map(Enum::name)
                .toList()
                .contains(updatedRegion)) {
            throw new NoSuchElementException(REGION_IS_NOT_EXISTS);
        }
        return Region.valueOf(updatedRegion);
    }
}
