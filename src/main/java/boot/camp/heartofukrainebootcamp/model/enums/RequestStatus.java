package boot.camp.heartofukrainebootcamp.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum RequestStatus {
    NEW("НОВИЙ"),
    PENDING("В ОЧІКУВАННІ"),
    IN_PROGRESS("В РОБОТІ"),
    DONE("ВИКОНАНИЙ"),
    CANCELED("СКАСОВАНИЙ");

    private final String uaRequestStatus;
}
