package boot.camp.heartofukrainebootcamp.model.enums;

public enum RoleName {
    USER,
    ADMIN
}
