package boot.camp.heartofukrainebootcamp.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.NoSuchElementException;

@Getter
@RequiredArgsConstructor
public enum RequestType {
    REQUEST("ЗАПИТ"),
    OFFER("ПРОПОЗИЦІЯ");

    private final String uaRequestType;

    private static final String REQUEST_TYPE_IS_NOT_EXISTS = "Such request type is not exists";

    public static RequestType getRequestType(String requestType) {
        String updatedRequestType = requestType.toUpperCase().strip();
        if (!Arrays.stream(RequestType.values())
                .map(Enum::name)
                .toList()
                .contains(updatedRequestType)) {
            throw new NoSuchElementException(REQUEST_TYPE_IS_NOT_EXISTS);
        }
        return RequestType.valueOf(updatedRequestType);
    }
}
