package boot.camp.heartofukrainebootcamp.repository;

import boot.camp.heartofukrainebootcamp.model.entity.ConfirmationToken;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, Long> {

    @Query("SELECT t FROM ConfirmationToken t LEFT JOIN FETCH t.user WHERE t.token = :token")
    Optional<ConfirmationToken> findByToken(String token);
}
