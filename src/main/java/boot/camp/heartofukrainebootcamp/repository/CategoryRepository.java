package boot.camp.heartofukrainebootcamp.repository;

import boot.camp.heartofukrainebootcamp.model.entity.Category;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    boolean existsByCategoryNameEn(String categoryNameEn);

    boolean existsByCategoryNameUa(String categoryNameUa);

    @EntityGraph(attributePaths = {"helpRequests"})
    Optional<Category> findById(Long id);
}
