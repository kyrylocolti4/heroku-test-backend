package boot.camp.heartofukrainebootcamp.repository;

import boot.camp.heartofukrainebootcamp.model.entity.Role;
import boot.camp.heartofukrainebootcamp.model.enums.RoleName;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName role);
}
