package boot.camp.heartofukrainebootcamp.repository.helprequest;

import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;
import java.util.Optional;

public interface HelpRequestRepository extends JpaRepository<HelpRequest, Long>, JpaSpecificationExecutor<HelpRequest> {

    @Query("SELECT hr FROM HelpRequest hr LEFT JOIN FETCH hr.images JOIN FETCH hr.categories WHERE hr.id = :id")
    Optional<HelpRequest> findById(@Param("id") Long id);

    @Query("SELECT hr FROM HelpRequest hr LEFT JOIN FETCH hr.images JOIN FETCH hr.categories")
    Page<HelpRequest> findAll(Pageable pageable);

    @Query("SELECT hr FROM HelpRequest hr LEFT JOIN FETCH hr.images JOIN FETCH hr.categories")
    List<HelpRequest> findAll();
}
