package boot.camp.heartofukrainebootcamp.repository.helprequest;

import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestSearchParameters;
import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import boot.camp.heartofukrainebootcamp.repository.spec.SpecificationBuilder;
import boot.camp.heartofukrainebootcamp.repository.spec.SpecificationProvidereManager;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class HelpRequestSpecificationBuilder implements SpecificationBuilder<HelpRequest> {

    private final SpecificationProvidereManager<HelpRequest> helpRequestSpecificationProvidereManager;

    @Override
    public Specification<HelpRequest> build(HelpRequestSearchParameters searchParameters) {
        Specification<HelpRequest> spec = Specification.where(null);
        if (searchParameters != null && searchParameters.regions().length > 0) {
            spec = spec.and(helpRequestSpecificationProvidereManager
                            .getSpecificationProvider("region")
                            .getSpecification(searchParameters.regions()));
        }

        if (searchParameters != null && searchParameters.categories().length > 0) {
            spec = spec.and(helpRequestSpecificationProvidereManager
                    .getSpecificationProvider("category")
                    .getSpecification(searchParameters.categories()));
        }
        return spec;
    }
}
