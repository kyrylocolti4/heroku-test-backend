package boot.camp.heartofukrainebootcamp.repository.helprequest;

import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import boot.camp.heartofukrainebootcamp.repository.spec.SpecificationProvider;
import boot.camp.heartofukrainebootcamp.repository.spec.SpecificationProvidereManager;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class HelpRequestSpecificationProviderManager implements SpecificationProvidereManager<HelpRequest> {
    private final List<SpecificationProvider<HelpRequest>> requestSpecificationProviders;

    @Override
    public SpecificationProvider<HelpRequest> getSpecificationProvider(String key) {
        return requestSpecificationProviders.stream()
                .filter(provider -> provider.getKey().equals(key))
                .findFirst()
                .orElseThrow(() ->
                        new RuntimeException("Can't find correct specification provider for key " + key));
    }
}
