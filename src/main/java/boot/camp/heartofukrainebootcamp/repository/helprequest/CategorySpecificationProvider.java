package boot.camp.heartofukrainebootcamp.repository.helprequest;

import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import boot.camp.heartofukrainebootcamp.repository.spec.SpecificationProvider;
import java.util.Arrays;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class CategorySpecificationProvider implements SpecificationProvider<HelpRequest> {
    @Override
    public String getKey() {
        return "category";
    }

    @Override
    public Specification<HelpRequest> getSpecification(String[] params) {
        return (root, query, criteriaBuilder) -> root
                .get("categories")
                .get("categoryNameEn")
                .in(Arrays.stream(params).toArray());
    }
}
