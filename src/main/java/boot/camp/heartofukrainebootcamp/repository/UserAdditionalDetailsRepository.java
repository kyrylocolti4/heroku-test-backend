package boot.camp.heartofukrainebootcamp.repository;

import boot.camp.heartofukrainebootcamp.model.entity.UserAdditionalDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAdditionalDetailsRepository extends JpaRepository<UserAdditionalDetails, Long> {
}
