package boot.camp.heartofukrainebootcamp.repository.spec;

public interface SpecificationProvidereManager<T> {
    SpecificationProvider<T> getSpecificationProvider(String key);
}
