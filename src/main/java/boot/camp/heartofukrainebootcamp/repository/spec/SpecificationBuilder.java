package boot.camp.heartofukrainebootcamp.repository.spec;

import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestSearchParameters;
import org.springframework.data.jpa.domain.Specification;

public interface SpecificationBuilder<T> {
    Specification<T> build(HelpRequestSearchParameters searchParameters);
}
