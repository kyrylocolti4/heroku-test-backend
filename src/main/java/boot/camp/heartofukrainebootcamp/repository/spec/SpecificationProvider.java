package boot.camp.heartofukrainebootcamp.repository.spec;

import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import org.springframework.data.jpa.domain.Specification;

public interface SpecificationProvider<T> {
    String getKey();
    Specification<T> getSpecification(String[] params);
}
