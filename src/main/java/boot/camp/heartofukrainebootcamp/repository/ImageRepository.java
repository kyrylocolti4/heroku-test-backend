package boot.camp.heartofukrainebootcamp.repository;

import boot.camp.heartofukrainebootcamp.model.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {
}
