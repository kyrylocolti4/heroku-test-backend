package boot.camp.heartofukrainebootcamp.repository;

import boot.camp.heartofukrainebootcamp.model.entity.ForgotPasswordToken;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ForgotPasswordTokenRepository extends JpaRepository<ForgotPasswordToken, Long> {

    @Query("SELECT t FROM ForgotPasswordToken t LEFT JOIN FETCH t.user WHERE t.token = :token")
    Optional<ForgotPasswordToken> findByToken(String token);
}
