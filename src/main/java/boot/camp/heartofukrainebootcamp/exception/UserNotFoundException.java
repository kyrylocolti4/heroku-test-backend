package boot.camp.heartofukrainebootcamp.exception;

public class UserNotFoundException extends RuntimeException {
    private static final String ERROR_MESSAGE = "User with email %s not found";

    public UserNotFoundException(String email) {
        super(ERROR_MESSAGE.formatted(email));
    }
}
