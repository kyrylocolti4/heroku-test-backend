package boot.camp.heartofukrainebootcamp.exception;

public class EmailConfirmationTokenExpiredException extends RuntimeException {
    private static final String ERROR_MESSAGE = "Token %s has expired";

    public EmailConfirmationTokenExpiredException(String token) {
        super(ERROR_MESSAGE.formatted(token));
    }
}
