package boot.camp.heartofukrainebootcamp.exception;

public class TokenNotFoundException extends RuntimeException {
    private static final String ERROR_MESSAGE = "Token %s not found";

    public TokenNotFoundException(String token) {
        super(ERROR_MESSAGE.formatted(token));
    }
}
