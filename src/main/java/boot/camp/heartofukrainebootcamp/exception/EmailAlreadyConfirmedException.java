package boot.camp.heartofukrainebootcamp.exception;

public class EmailAlreadyConfirmedException extends RuntimeException {
    private static final String ERROR_MESSAGE = "Email %s is already confirmed";

    public EmailAlreadyConfirmedException(String email) {
        super(ERROR_MESSAGE.formatted(email));
    }
}
