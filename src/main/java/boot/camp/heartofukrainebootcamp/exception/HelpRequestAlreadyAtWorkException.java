package boot.camp.heartofukrainebootcamp.exception;

public class HelpRequestAlreadyAtWorkException extends RuntimeException {

    public HelpRequestAlreadyAtWorkException(String message) {
        super(message);
    }
}