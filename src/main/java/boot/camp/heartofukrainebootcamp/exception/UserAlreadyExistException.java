package boot.camp.heartofukrainebootcamp.exception;

public class UserAlreadyExistException extends RuntimeException {
    private static final String ERROR_MESSAGE = "User with email %s already exists";

    public UserAlreadyExistException(String email) {
        super(ERROR_MESSAGE.formatted(email));
    }
}