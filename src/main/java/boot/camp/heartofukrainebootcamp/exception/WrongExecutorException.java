package boot.camp.heartofukrainebootcamp.exception;

public class WrongExecutorException extends RuntimeException {

    public WrongExecutorException(String message) {
        super(message);
    }
}