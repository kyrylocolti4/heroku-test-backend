package boot.camp.heartofukrainebootcamp.exception;

public class NotEmptyCategoryException extends RuntimeException {

    public NotEmptyCategoryException(String message) {
        super(message);
    }
}