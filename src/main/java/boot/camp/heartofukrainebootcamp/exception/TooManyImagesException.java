package boot.camp.heartofukrainebootcamp.exception;

public class TooManyImagesException extends RuntimeException {

    public TooManyImagesException(String message) {
        super(message);
    }
}