package boot.camp.heartofukrainebootcamp.mapper;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestStatusDto;
import boot.camp.heartofukrainebootcamp.model.enums.RequestStatus;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RequestStatusMapper {
    default RequestStatusDto toDto(RequestStatus requestStatus) {
        return new RequestStatusDto(requestStatus.name(), requestStatus.getUaRequestStatus());
    }
}
