package boot.camp.heartofukrainebootcamp.mapper;

import boot.camp.heartofukrainebootcamp.config.MapperConfig;
import boot.camp.heartofukrainebootcamp.dto.user.UserResponseDto;
import boot.camp.heartofukrainebootcamp.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapperConfig.class, uses = {RegionMapper.class})
public interface UserMapper {

    UserResponseDto toResponseDto(User user);
}
