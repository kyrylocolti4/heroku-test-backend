package boot.camp.heartofukrainebootcamp.mapper;

import boot.camp.heartofukrainebootcamp.dto.helprequest.CategoryDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestDto;
import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import boot.camp.heartofukrainebootcamp.model.entity.Image;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


//TODO change HelpRequestMapper using MapStruct
@Service
@RequiredArgsConstructor
public class HelpRequestMapper {
    private final CategoryMapper categoryMapper;
    private final RegionMapper regionMapper;
    private final RequestTypeMapper requestTypeMapper;
    private final RequestStatusMapper requestStatusMapper;

    public HelpRequestDto toDto(HelpRequest helpRequest) {
        String executorEmail = null;
        if(helpRequest.getExecutor() != null) {
            executorEmail = helpRequest.getExecutor().getUsername();
        }

        List<Long> images = null;
        if(helpRequest.getImages() != null) {
            images = helpRequest.getImages()
                    .stream()
                    .map(Image::getId)
                    .toList();
        }

        Set<CategoryDto> categoryDtos = helpRequest.getCategories().stream()
                .map(categoryMapper::toDto)
                .collect(Collectors.toSet());

        return HelpRequestDto
                .builder()
                .id(helpRequest.getId())
                .title(helpRequest.getTitle())
                .categories(categoryDtos)
                .description(helpRequest.getDescription())
                .createdAt(helpRequest.getCreatedAt())
                .region(regionMapper.toDto(helpRequest.getRegion()))
                .requestType(requestTypeMapper.toDto(helpRequest.getType()))
                .requestStatus(requestStatusMapper.toDto(helpRequest.getStatus()))
                .authorEmail(helpRequest.getAuthor().getUsername())
                .executorEmail(executorEmail)
                .comment(helpRequest.getComment())
                .images(images)
                .build();
    }
}
