package boot.camp.heartofukrainebootcamp.mapper;

import boot.camp.heartofukrainebootcamp.config.MapperConfig;
import boot.camp.heartofukrainebootcamp.dto.region.RegionDto;
import boot.camp.heartofukrainebootcamp.model.enums.Region;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(config = MapperConfig.class)
public interface RegionMapper {

    @Mappings({
            @Mapping(target = "regionEn", source = "region"),
            @Mapping(target = "regionUa", source = "region.uaRegion")
    })
    RegionDto toDto(Region region);
}
