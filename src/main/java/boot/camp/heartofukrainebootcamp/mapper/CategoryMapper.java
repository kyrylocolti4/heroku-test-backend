package boot.camp.heartofukrainebootcamp.mapper;

import boot.camp.heartofukrainebootcamp.dto.helprequest.CategoryDto;
import boot.camp.heartofukrainebootcamp.model.entity.Category;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    CategoryDto toDto (Category category);

    Category toEntity (CategoryDto categoryDto);
}
