package boot.camp.heartofukrainebootcamp.mapper;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestTypeDto;
import boot.camp.heartofukrainebootcamp.model.enums.RequestType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RequestTypeMapper {
    default RequestTypeDto toDto(RequestType requestType) {
        return new RequestTypeDto(requestType.name(), requestType.getUaRequestType());
    }
}
