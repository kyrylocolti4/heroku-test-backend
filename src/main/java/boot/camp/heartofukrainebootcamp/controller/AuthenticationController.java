package boot.camp.heartofukrainebootcamp.controller;

import boot.camp.heartofukrainebootcamp.dto.email.EmailRequestDto;
import boot.camp.heartofukrainebootcamp.dto.user.LoginRequestDto;
import boot.camp.heartofukrainebootcamp.dto.user.LoginResponseDto;
import boot.camp.heartofukrainebootcamp.dto.user.RegistrationRequestDto;
import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import boot.camp.heartofukrainebootcamp.security.AuthenticationService;
import boot.camp.heartofukrainebootcamp.service.RegistrationService;
import boot.camp.heartofukrainebootcamp.util.swagger.AuthenticationControllerOpenApi;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController implements AuthenticationControllerOpenApi {
    private final RegistrationService registrationService;
    private final AuthenticationService authenticationService;

    @Value("${frontend.login.page.url}")
    private String frontendLoginPageUrl;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void register(@RequestBody @Valid RegistrationRequestDto request) {
        registrationService.register(request);
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public LoginResponseDto login(@RequestBody LoginRequestDto request) {
        return authenticationService.authenticate(request);
    }

    @GetMapping("/confirm")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Void> confirm(@RequestParam("token") String token) {
        registrationService.confirmToken(token);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", frontendLoginPageUrl);
        return new ResponseEntity<>(headers, HttpStatus.FOUND);
    }

    @PostMapping("/resend-email")
    public void resendConfirmationEmail(@RequestBody EmailRequestDto request) {
        registrationService.sendConfirmationEmail(request.getEmail());
    }
}
