package boot.camp.heartofukrainebootcamp.controller;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestStatusDto;
import boot.camp.heartofukrainebootcamp.service.RequestStatusService;
import boot.camp.heartofukrainebootcamp.util.swagger.RequestStatusControllerOpenApi;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/request-statuses")
@RequiredArgsConstructor
public class RequestStatusController implements RequestStatusControllerOpenApi {
    private final RequestStatusService requestStatusService;

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<RequestStatusDto> findAll() {
        return requestStatusService.findAll();
    }
}
