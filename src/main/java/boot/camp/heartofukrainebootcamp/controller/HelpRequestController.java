package boot.camp.heartofukrainebootcamp.controller;

import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestRequestDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestSearchParameters;
import boot.camp.heartofukrainebootcamp.service.HelpRequestService;
import boot.camp.heartofukrainebootcamp.util.swagger.HelpRequestControllerOpenApi;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/help-requests")
@RequiredArgsConstructor
public class HelpRequestController implements HelpRequestControllerOpenApi {
    private final HelpRequestService helpRequestService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public HelpRequestDto save(@Valid @RequestBody HelpRequestRequestDto request) {
        return helpRequestService.save(request);
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public Page<HelpRequestDto> findAll(Pageable pageable) {
        return helpRequestService.findAll(pageable);
    }

    @GetMapping("/search")
    public List<HelpRequestDto> search(HelpRequestSearchParameters parameters) {
        return helpRequestService.search(parameters);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable("id") Long id,
                       @Valid @RequestBody HelpRequestRequestDto helpRequestRequest) {
        helpRequestService.update(id, helpRequestRequest);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void setExecutor(@PathVariable("id") Long id) {
        helpRequestService.setExecutor(id);
    }

    @PutMapping(value = "/{id}/images", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void addImages(@PathVariable("id") Long id,
                          @RequestParam("images") MultipartFile[] files) throws IOException {
        helpRequestService.addImages(id, files);
    }
}
