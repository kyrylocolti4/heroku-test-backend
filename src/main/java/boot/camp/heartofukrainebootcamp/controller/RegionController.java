package boot.camp.heartofukrainebootcamp.controller;

import boot.camp.heartofukrainebootcamp.dto.region.RegionDto;
import boot.camp.heartofukrainebootcamp.service.RegionService;
import boot.camp.heartofukrainebootcamp.util.swagger.RegionControllerOpenApi;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/regions")
@RequiredArgsConstructor
public class RegionController implements RegionControllerOpenApi {
    private final RegionService regionService;

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<RegionDto> findAll() {
        return regionService.findAll();
    }
}
