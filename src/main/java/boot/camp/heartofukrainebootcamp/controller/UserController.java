package boot.camp.heartofukrainebootcamp.controller;

import boot.camp.heartofukrainebootcamp.dto.email.EmailRequestDto;
import boot.camp.heartofukrainebootcamp.dto.user.UserResponseDto;
import boot.camp.heartofukrainebootcamp.dto.user.password.ChangePasswordRequestDto;
import boot.camp.heartofukrainebootcamp.dto.user.password.ForgotPasswordRequestDto;
import boot.camp.heartofukrainebootcamp.service.UserService;
import boot.camp.heartofukrainebootcamp.util.swagger.UserControllerOpenApi;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController implements UserControllerOpenApi {
    private final UserService userService;

    @PutMapping("/change-password")
    @ResponseStatus(HttpStatus.OK)
    public void changePassword(@RequestBody ChangePasswordRequestDto request) {
        userService.changePassword(request);
    }

    @PostMapping("/forgot-password")
    @ResponseStatus(HttpStatus.OK)
    public void forgotPassword(@RequestBody EmailRequestDto request) {
        userService.sendForgotPasswordEmail(request.getEmail());
    }

    @PutMapping("/reset-password")
    @ResponseStatus(HttpStatus.OK)
    public void resetPassword(@RequestBody ForgotPasswordRequestDto request,
                              @RequestParam("token") String token) {
        userService.forgotPassword(request.getNewPassword(), token);
    }

    @GetMapping("/current")
    @ResponseStatus(HttpStatus.OK)
    public UserResponseDto getCurrentUser() {
        return userService.getCurrentAuthenticatedUserDto();
    }
}
