package boot.camp.heartofukrainebootcamp.controller;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestTypeDto;
import boot.camp.heartofukrainebootcamp.service.RequestTypeService;
import boot.camp.heartofukrainebootcamp.util.swagger.RequestTypeControllerOpenApi;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/request-types")
@RequiredArgsConstructor
public class RequestTypeController implements RequestTypeControllerOpenApi {
    private final RequestTypeService requestTypeService;

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<RequestTypeDto> findAll() {
        return requestTypeService.findAll();
    }
}
