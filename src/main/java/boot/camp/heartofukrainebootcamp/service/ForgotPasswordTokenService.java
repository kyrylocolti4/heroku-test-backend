package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.model.entity.ForgotPasswordToken;
import boot.camp.heartofukrainebootcamp.model.entity.User;

public interface ForgotPasswordTokenService {
    ForgotPasswordToken generateToken(User user);

    String saveToken(ForgotPasswordToken forgotPasswordToken);

    ForgotPasswordToken getToken(String token);

    void validateForgotPasswordToken(String forgotPasswordToken);
}
