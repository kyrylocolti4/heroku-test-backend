package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestRequestDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestSearchParameters;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface HelpRequestService {
    HelpRequestDto save(HelpRequestRequestDto helpRequestRequest);

    List<HelpRequestDto> search(HelpRequestSearchParameters params);

    void update(Long id, HelpRequestRequestDto helpRequestRequest);

    Page<HelpRequestDto> findAll(Pageable pageable);

    void setExecutor(Long id);

    void addImages(Long id, MultipartFile[] files) throws IOException;
}
