package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.dto.helprequest.CategoryDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.CategoryRequestDto;
import boot.camp.heartofukrainebootcamp.model.entity.Category;

import java.util.List;
import java.util.Set;

public interface CategoryService {
    CategoryDto save(CategoryRequestDto categoryRequest);

    List<CategoryDto> findAll();

    CategoryDto findById(Long id);

    void update(Long id, CategoryRequestDto categoryRequest);

    void deleteById(Long id);

    Set<Category> getCategoriesByIds(Set<Long> categoryIds);
}
