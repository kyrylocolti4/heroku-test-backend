package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.dto.region.RegionDto;
import java.util.List;

public interface RegionService {
    List<RegionDto> findAll();
}
