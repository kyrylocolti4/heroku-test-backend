package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.dto.user.RegistrationRequestDto;

public interface RegistrationService {

    void register(RegistrationRequestDto request);

    void confirmToken(String token);

    void sendConfirmationEmail(String email);

}
