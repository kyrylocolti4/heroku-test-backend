package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestTypeDto;
import java.util.List;

public interface RequestTypeService {
    List<RequestTypeDto> findAll();
}
