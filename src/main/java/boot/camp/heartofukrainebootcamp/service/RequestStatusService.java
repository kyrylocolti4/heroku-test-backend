package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestStatusDto;
import java.util.List;

public interface RequestStatusService {
    List<RequestStatusDto> findAll();
}
