package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.model.entity.ConfirmationToken;
import boot.camp.heartofukrainebootcamp.model.entity.User;

public interface ConfirmationTokenService {
    String saveConfirmationToken(ConfirmationToken token);

    ConfirmationToken getToken(String token);

    ConfirmationToken generateToken(User user);
}
