package boot.camp.heartofukrainebootcamp.service;

import java.io.IOException;

import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import boot.camp.heartofukrainebootcamp.model.entity.Image;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {
    Image uploadImage(MultipartFile file, HelpRequest helpRequest) throws IOException;

    byte[] downloadImage(Long id);
}