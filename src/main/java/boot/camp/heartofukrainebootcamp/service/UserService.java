package boot.camp.heartofukrainebootcamp.service;

import boot.camp.heartofukrainebootcamp.dto.user.UserResponseDto;
import boot.camp.heartofukrainebootcamp.dto.user.password.ChangePasswordRequestDto;
import boot.camp.heartofukrainebootcamp.model.entity.User;

public interface UserService {
    void enableUser(String email);

    void changePassword(ChangePasswordRequestDto request);

    User getCurrentAuthenticatedUser();

    void forgotPassword(String newPassword, String token);

    void sendForgotPasswordEmail(String email);

    UserResponseDto getCurrentAuthenticatedUserDto();
}
