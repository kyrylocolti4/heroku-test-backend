package boot.camp.heartofukrainebootcamp.service;

public interface EmailSenderService {

    void send(String to, String message, String subject);
}
