package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.dto.user.UserResponseDto;
import boot.camp.heartofukrainebootcamp.dto.user.password.ChangePasswordRequestDto;
import boot.camp.heartofukrainebootcamp.exception.InvalidPasswordException;
import boot.camp.heartofukrainebootcamp.exception.UserNotAuthenticatedException;
import boot.camp.heartofukrainebootcamp.exception.UserNotFoundException;
import boot.camp.heartofukrainebootcamp.mapper.UserMapper;
import boot.camp.heartofukrainebootcamp.model.entity.ForgotPasswordToken;
import boot.camp.heartofukrainebootcamp.model.entity.User;
import boot.camp.heartofukrainebootcamp.repository.UserRepository;
import boot.camp.heartofukrainebootcamp.security.oauth2.CustomOAuth2User;
import boot.camp.heartofukrainebootcamp.service.EmailSenderService;
import boot.camp.heartofukrainebootcamp.service.ForgotPasswordTokenService;
import boot.camp.heartofukrainebootcamp.service.UserService;
import boot.camp.heartofukrainebootcamp.util.EmailUtil;

import java.time.LocalDateTime;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private static final String FORGOT_PASSWORD_EMAIL_SUBJECT = "Reset password instruction";
    private static final String TOKEN_CONFIRMATION_URL = "http://localhost:8080/api/v1/users/reset-password?token=";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ForgotPasswordTokenService forgotPasswordTokenService;
    private final EmailSenderService emailSenderService;
    private final UserMapper userMapper;

    @Override
    public void enableUser(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(email));
        user.setEnabled(true);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void forgotPassword(String newPassword, String forgotPasswordToken) {
        ForgotPasswordToken token = forgotPasswordTokenService.getToken(forgotPasswordToken);
        forgotPasswordTokenService.validateForgotPasswordToken(token.getToken());
        token.setConfirmedAt(LocalDateTime.now());

        User user = token.getUser();
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(user);
    }

    @Override
    public void changePassword(ChangePasswordRequestDto request) {
        User user = getCurrentAuthenticatedUser();
        if (!passwordEncoder.matches(request.getOldPassword(), user.getPassword())) {
            throw new InvalidPasswordException("Old password field do not match user password");
        }
        user.setPassword(passwordEncoder.encode(request.getNewPassword()));
        userRepository.save(user);
    }

    @Override
    public User getCurrentAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            String email;
            if (authentication.getPrincipal() instanceof UserDetails userDetails) {
                email = userDetails.getUsername();
            } else if (authentication.getPrincipal() instanceof CustomOAuth2User customOAuth2User) {
                email = customOAuth2User.getEmail();
            } else {
                email = null;
            }

            if (email != null) {
                return userRepository
                        .findByEmail(email)
                        .orElseThrow(() -> new UserNotFoundException(email));
            }
        }
        throw new UserNotAuthenticatedException("User is not authenticated");
    }

    @Override
    public UserResponseDto getCurrentAuthenticatedUserDto() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            String email;
            if (authentication.getPrincipal() instanceof UserDetails userDetails) {
                email = userDetails.getUsername();
            } else if (authentication.getPrincipal() instanceof CustomOAuth2User customOAuth2User) {
                email = customOAuth2User.getEmail();
            } else {
                email = null;
            }

            if (email != null) {
                User user = userRepository
                        .findByEmail(email)
                        .orElseThrow(() -> new UserNotFoundException(email));

                return userMapper.toResponseDto(user);
            }
        }
        throw new UserNotAuthenticatedException("User is not authenticated");
    }

    @Override
    public void sendForgotPasswordEmail(String email) {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(email));

        ForgotPasswordToken forgotPasswordToken = forgotPasswordTokenService.generateToken(user);
        String token = forgotPasswordTokenService.saveToken(forgotPasswordToken);

        String link = TOKEN_CONFIRMATION_URL + token;
        emailSenderService.send(
                user.getEmail().toLowerCase(),
                EmailUtil.buildForgotPasswordTokenConfirmationMessage(user.getFirstName(), link),
                FORGOT_PASSWORD_EMAIL_SUBJECT);
    }
}
