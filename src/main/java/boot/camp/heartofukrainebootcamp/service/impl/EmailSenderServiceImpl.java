package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.exception.FailedToSendEmailException;
import boot.camp.heartofukrainebootcamp.service.EmailSenderService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailSenderServiceImpl implements EmailSenderService {
    private static final String MAIL_ENCODING = "utf-8";
    private final JavaMailSender mailSender;

    @Value("${email.sender.login}")
    private String sender;

    @Override
    @Async
    public void send(String to, String email, String subject) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, MAIL_ENCODING);
            helper.setText(email, true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setFrom(sender);
            mailSender.send(message);
        } catch (MessagingException e) {
            log.error("Failed to send email", e);
            throw new FailedToSendEmailException("Failed to send email " + e);
        }
    }
}
