package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.dto.helprequest.CategoryDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.CategoryRequestDto;
import boot.camp.heartofukrainebootcamp.exception.CategoryAlreadyExistException;
import boot.camp.heartofukrainebootcamp.exception.NotEmptyCategoryException;
import boot.camp.heartofukrainebootcamp.mapper.CategoryMapper;
import boot.camp.heartofukrainebootcamp.model.entity.Category;
import boot.camp.heartofukrainebootcamp.repository.CategoryRepository;
import boot.camp.heartofukrainebootcamp.service.CategoryService;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    private static final String CATEGORY_IS_NOT_EXISTS = "Such category is not exists";
    private static final String CATEGORY_IS_ALREADY_EXISTS = "Such category is already exists";
    private static final String CATEGORY_CONTAINS_HELP_REQUESTS =
            "There are help requests at this category. At first delete help requests";


    @Override
    public CategoryDto save(CategoryRequestDto categoryRequest) {
        String categoryNameEn = updateCategoryName(categoryRequest.getCategoryNameEn());
        String categoryNameUa = updateCategoryName(categoryRequest.getCategoryNameUa());
        if (categoryRepository.existsByCategoryNameEn(categoryNameEn)||
                categoryRepository.existsByCategoryNameUa(categoryNameUa)) {
            throw new CategoryAlreadyExistException(CATEGORY_IS_ALREADY_EXISTS);
        }

        Category newCategory = new Category();
        newCategory.setCategoryNameEn(categoryNameEn);
        newCategory.setCategoryNameUa(categoryNameUa);
        return categoryMapper.toDto(categoryRepository.save(newCategory));
    }

    @Override
    public List<CategoryDto> findAll() {
        return categoryRepository.findAll().stream()
                .map(categoryMapper::toDto)
                .toList();
    }

    @Override
    public CategoryDto findById(Long id) {
        return categoryMapper.toDto(categoryRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException(CATEGORY_IS_NOT_EXISTS)));
    }

    @Override
    public void update(Long id, CategoryRequestDto categoryRequest) {
        Category categoryToUpdate = categoryRepository.findById(id).orElseThrow(
                () -> new NoSuchElementException(CATEGORY_IS_NOT_EXISTS));

        String categoryNameEn = updateCategoryName(categoryRequest.getCategoryNameEn());
        String categoryNameUa = updateCategoryName(categoryRequest.getCategoryNameUa());
        if (categoryRepository.existsByCategoryNameEn(categoryNameEn)||
                categoryRepository.existsByCategoryNameUa(categoryNameUa)) {
            throw new CategoryAlreadyExistException(CATEGORY_IS_ALREADY_EXISTS);
        }

        categoryToUpdate.setCategoryNameEn(categoryNameEn);
        categoryToUpdate.setCategoryNameUa(categoryNameUa);
        categoryRepository.save(categoryToUpdate);
    }

    @Override
    public void deleteById(Long id) {
        if (categoryRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException(CATEGORY_IS_NOT_EXISTS))
                .getHelpRequests()
                .isEmpty()) {
            categoryRepository.deleteById(id);
        } else {
            throw new NotEmptyCategoryException(CATEGORY_CONTAINS_HELP_REQUESTS);
        }

    }

    @Override
    public Set<Category> getCategoriesByIds(Set<Long> categoryIds) {
        if (categoryIds == null) {
            return null;
        }
        return categoryIds.stream()
                .map(id -> categoryMapper.toEntity(findById(id)))
                .collect(Collectors.toSet());
    }

    private String updateCategoryName(String categoryName) {
        String stripCategoryName = categoryName.strip();
        return stripCategoryName.substring(0, 1).toUpperCase() + stripCategoryName.substring(1).toLowerCase();
    }
}
