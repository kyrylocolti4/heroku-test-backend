package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestStatusDto;
import boot.camp.heartofukrainebootcamp.mapper.RequestStatusMapper;
import boot.camp.heartofukrainebootcamp.model.enums.RequestStatus;
import boot.camp.heartofukrainebootcamp.service.RequestStatusService;
import java.util.Arrays;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RequestStatusServiceImpl implements RequestStatusService {
    private final RequestStatusMapper requestStatusMapper;

    @Override
    public List<RequestStatusDto> findAll() {
        return Arrays.stream(RequestStatus.values()).toList()
                .stream()
                .map(requestStatusMapper::toDto)
                .toList();
    }
}