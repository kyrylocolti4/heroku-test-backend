package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestTypeDto;
import boot.camp.heartofukrainebootcamp.mapper.RequestTypeMapper;
import boot.camp.heartofukrainebootcamp.model.enums.RequestType;
import boot.camp.heartofukrainebootcamp.service.RequestTypeService;
import java.util.Arrays;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RequestTypeServiceImpl implements RequestTypeService {
    private final RequestTypeMapper requestTypeMapper;

    @Override
    public List<RequestTypeDto> findAll() {
        return Arrays.stream(RequestType.values()).toList()
                .stream()
                .map(requestTypeMapper::toDto)
                .toList();
    }
}