package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestRequestDto;
import boot.camp.heartofukrainebootcamp.exception.HelpRequestAlreadyAtWorkException;
import boot.camp.heartofukrainebootcamp.exception.TooManyImagesException;
import boot.camp.heartofukrainebootcamp.exception.WrongExecutorException;
import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestSearchParameters;
import boot.camp.heartofukrainebootcamp.mapper.HelpRequestMapper;
import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import boot.camp.heartofukrainebootcamp.model.entity.User;
import boot.camp.heartofukrainebootcamp.model.enums.Region;
import boot.camp.heartofukrainebootcamp.model.enums.RequestStatus;
import boot.camp.heartofukrainebootcamp.model.enums.RequestType;
import boot.camp.heartofukrainebootcamp.model.enums.RoleName;
import boot.camp.heartofukrainebootcamp.service.CategoryService;
import boot.camp.heartofukrainebootcamp.repository.helprequest.HelpRequestRepository;
import boot.camp.heartofukrainebootcamp.repository.helprequest.HelpRequestSpecificationBuilder;
import boot.camp.heartofukrainebootcamp.service.HelpRequestService;
import boot.camp.heartofukrainebootcamp.service.ImageService;
import boot.camp.heartofukrainebootcamp.service.UserService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class HelpRequestServiceImpl implements HelpRequestService {
    private final HelpRequestRepository helpRequestRepository;
    private final HelpRequestMapper helpRequestMapper;
    private final UserService userService;
    private final CategoryService categoryService;
    private final ImageService imageService;
    private final HelpRequestSpecificationBuilder helpRequestSpecificationBuilder;

    private static final int MAX_IMAGES_COUNT = 4;
    private static final String HELP_REQUEST_IS_NOT_EXISTS = "Such help request is not exists";
    private static final String ACCESS_FORBIDDEN = "Access forbidden";
    private static final String HELP_REQUEST_IS_ALREADY_AT_WORK = "%s is already at work";
    private static final String WRONG_EXECUTOR = "Author can't be executor";
    private static final String TOO_MANY_IMAGES = "You can upload maximum %d images for 1 help request";


    @Override
    public HelpRequestDto save(HelpRequestRequestDto helpRequestRequest) {
        User author = userService.getCurrentAuthenticatedUser();

        HelpRequest newHelpRequest = HelpRequest.builder()
                .title(helpRequestRequest.getTitle())
                .categories(categoryService.getCategoriesByIds(helpRequestRequest.getCategoryIds()))
                .description(helpRequestRequest.getDescription())
                .createdAt(LocalDateTime.now())
                .region(Region.getRegion(helpRequestRequest.getRegion()))
                .type(RequestType.getRequestType(helpRequestRequest.getRequestType()))
                .status(RequestStatus.NEW)
                .author(author)
                .comment(helpRequestRequest.getComment())
                .build();
        return helpRequestMapper.toDto(helpRequestRepository.save(newHelpRequest));
    }

    @Override
    public Page<HelpRequestDto> findAll(Pageable pageable) {
        Page<HelpRequest> requests = helpRequestRepository.findAll(pageable);
        return requests.map(helpRequestMapper::toDto);
    }

    @Override
    public void update(Long id, HelpRequestRequestDto helpRequestRequest) {
        HelpRequest helpRequestToUpdate = helpRequestRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException(HELP_REQUEST_IS_NOT_EXISTS));

        User currentUser = userService.getCurrentAuthenticatedUser();
        if (!helpRequestToUpdate.getAuthor().getEmail().equals(currentUser.getEmail()) &&
                currentUser.getRoles()
                        .stream()
                        .noneMatch(role -> role.getName().equals(RoleName.ADMIN))) {
            throw new AccessDeniedException(ACCESS_FORBIDDEN);
        }

        helpRequestToUpdate.setTitle(helpRequestRequest.getTitle());
        helpRequestToUpdate.setDescription(helpRequestRequest.getDescription());
        helpRequestToUpdate.setCategories(categoryService.getCategoriesByIds(helpRequestRequest.getCategoryIds()));
        helpRequestToUpdate.setRegion(Region.getRegion(helpRequestRequest.getRegion()));
        helpRequestToUpdate.setType(RequestType.getRequestType(helpRequestRequest.getRequestType()));
        helpRequestToUpdate.setComment(helpRequestRequest.getComment());
        helpRequestRepository.save(helpRequestToUpdate);
    }

    @Override
    @Transactional
    public List<HelpRequestDto> search(HelpRequestSearchParameters params) {
        Specification<HelpRequest> helpRequestSpecification = helpRequestSpecificationBuilder.build(params);
        return helpRequestRepository.findAll(helpRequestSpecification)
                .stream()
                .map(helpRequestMapper::toDto)
                .toList();
    }

    @Override
    public void setExecutor(Long id) {
        HelpRequest helpRequest = helpRequestRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException(HELP_REQUEST_IS_NOT_EXISTS));

        if (helpRequest.getExecutor() != null) {
            throw new HelpRequestAlreadyAtWorkException(HELP_REQUEST_IS_ALREADY_AT_WORK
                    .formatted(helpRequest.getType().name()));
        }
        if (helpRequest.getAuthor().getId().equals(userService.getCurrentAuthenticatedUser().getId())) {
            throw new WrongExecutorException(WRONG_EXECUTOR);
        }

        helpRequest.setStatus(RequestStatus.IN_PROGRESS);
        helpRequest.setExecutor(userService.getCurrentAuthenticatedUser());
        helpRequestRepository.save(helpRequest);
    }

    @Override
    @Transactional
    public void addImages(Long id, MultipartFile[] files) throws IOException {
        HelpRequest helpRequest = helpRequestRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException(HELP_REQUEST_IS_NOT_EXISTS));

        if ((helpRequest.getImages().size() + files.length) > MAX_IMAGES_COUNT) {
            throw new TooManyImagesException(TOO_MANY_IMAGES.formatted(MAX_IMAGES_COUNT));
        }

        for (MultipartFile file : files) {
            imageService.uploadImage(file, helpRequest);
        }
    }
}
