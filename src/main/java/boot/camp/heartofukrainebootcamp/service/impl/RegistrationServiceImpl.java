package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.dto.user.RegistrationRequestDto;
import boot.camp.heartofukrainebootcamp.exception.EmailAlreadyConfirmedException;
import boot.camp.heartofukrainebootcamp.exception.EmailConfirmationTokenExpiredException;
import boot.camp.heartofukrainebootcamp.exception.FailedToSendEmailException;
import boot.camp.heartofukrainebootcamp.exception.UserAlreadyExistException;
import boot.camp.heartofukrainebootcamp.exception.UserNotFoundException;
import boot.camp.heartofukrainebootcamp.model.entity.ConfirmationToken;
import boot.camp.heartofukrainebootcamp.model.entity.User;
import boot.camp.heartofukrainebootcamp.model.entity.UserAdditionalDetails;
import boot.camp.heartofukrainebootcamp.repository.UserRepository;
import boot.camp.heartofukrainebootcamp.service.ConfirmationTokenService;
import boot.camp.heartofukrainebootcamp.service.EmailSenderService;
import boot.camp.heartofukrainebootcamp.service.RegistrationService;
import boot.camp.heartofukrainebootcamp.service.UserService;
import boot.camp.heartofukrainebootcamp.util.EmailUtil;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {
    private static final String EMAIL_SUBJECT = "Email Confirmation";
    private static final String TOKEN_CONFIRMATION_BASE_URL = "http://localhost:8080/api/v1/auth/confirm?token=";

    private final UserRepository userRepository;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final ConfirmationTokenService confirmationTokenService;
    private final EmailSenderService emailSenderService;

    @Override
    @Transactional
    public void register(RegistrationRequestDto request) {
        if (userRepository.findByEmail(request.getEmail().toLowerCase()).isPresent()) {
            throw new UserAlreadyExistException(request.getEmail());
        }
        User user = User.builder()
                .email(request.getEmail().toLowerCase())
                .password(passwordEncoder.encode(request.getPassword()))
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .userAdditionalDetails(new UserAdditionalDetails())
                .build();
        userRepository.save(user);

        sendConfirmationEmail(request.getEmail());
    }

    @Override
    @Transactional
    public void confirmToken(String token) {
        ConfirmationToken confirmationToken = confirmationTokenService.getToken(token);

        if (confirmationToken.getConfirmedAt() != null) {
            throw new EmailAlreadyConfirmedException(confirmationToken.getUser().getEmail());
        }

        if (confirmationToken.getExpiresAt().isBefore(LocalDateTime.now())) {
            throw new EmailConfirmationTokenExpiredException(token);
        }
        confirmationToken.setConfirmedAt(LocalDateTime.now());
        userService.enableUser(confirmationToken.getUser().getEmail());
    }

    @Override
    public void sendConfirmationEmail(String email) {
        User user = userRepository.findByEmail(email.toLowerCase())
                .orElseThrow(() -> new UserNotFoundException(email.toLowerCase()));

        if (user.isEnabled()) {
            throw new FailedToSendEmailException("User email is already verified");
        }

        ConfirmationToken confirmationToken = confirmationTokenService.generateToken(user);
        String token = confirmationTokenService.saveConfirmationToken(confirmationToken);

        String link = TOKEN_CONFIRMATION_BASE_URL + token;
        emailSenderService.send(
                user.getEmail().toLowerCase(),
                EmailUtil.buildEmailTokenConfirmationMessage(user.getFirstName(), link),
                EMAIL_SUBJECT);
    }
}
