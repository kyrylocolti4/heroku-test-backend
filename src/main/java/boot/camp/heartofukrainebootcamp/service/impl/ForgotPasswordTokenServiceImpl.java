package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.exception.TokenNotFoundException;
import boot.camp.heartofukrainebootcamp.model.entity.ForgotPasswordToken;
import boot.camp.heartofukrainebootcamp.model.entity.User;
import boot.camp.heartofukrainebootcamp.repository.ForgotPasswordTokenRepository;
import boot.camp.heartofukrainebootcamp.service.ForgotPasswordTokenService;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ForgotPasswordTokenServiceImpl implements ForgotPasswordTokenService {
    private final ForgotPasswordTokenRepository forgotPasswordTokenRepository;

    @Value("${forgot.password.token.lifetime}")
    private Long tokenLifetime;

    @Override
    public ForgotPasswordToken generateToken(User user) {
        return ForgotPasswordToken.builder()
                .token(UUID.randomUUID().toString())
                .createdAt(LocalDateTime.now())
                .expiresAt(LocalDateTime.now().plusMinutes(tokenLifetime))
                .user(user)
                .build();
    }

    @Override
    public String saveToken(ForgotPasswordToken forgotPasswordToken) {
        return forgotPasswordTokenRepository.save(forgotPasswordToken).getToken();
    }

    @Override
    public ForgotPasswordToken getToken(String token) {
        return forgotPasswordTokenRepository
                .findByToken(token)
                .orElseThrow(() -> new TokenNotFoundException(token));
    }

    @Override
    public void validateForgotPasswordToken(String forgotPasswordToken) {
        ForgotPasswordToken token = getToken(forgotPasswordToken);

        if (token.getConfirmedAt() != null) {
            throw new IllegalStateException("Token has already been already used");
        }

        if (token.getExpiresAt().isBefore(LocalDateTime.now())) {
            throw new IllegalStateException("Token has expired");
        }
        token.setConfirmedAt(LocalDateTime.now());
    }
}
