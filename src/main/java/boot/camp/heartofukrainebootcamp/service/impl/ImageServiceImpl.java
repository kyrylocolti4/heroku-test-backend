package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.model.entity.HelpRequest;
import boot.camp.heartofukrainebootcamp.model.entity.Image;
import boot.camp.heartofukrainebootcamp.repository.ImageRepository;
import boot.camp.heartofukrainebootcamp.service.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepository;

    private static final String FILE_NOT_PROVIDE = "You should provide a valid file";
    private static final String IMAGE_NOT_FOUND = "Image not found";
    private static final String NOT_SUPPORTED = "File format is not supported. " +
            "Supported file formats: jpeg, jpg, png";
    private static final List<String> ALLOWED_FILE_EXTENSIONS = Arrays.asList("image/png", "image/jpeg", "image/jpg");

    @Override
    public Image uploadImage(MultipartFile file, HelpRequest helpRequest) throws IOException {
        if (file.isEmpty()) {
            throw new FileNotFoundException(FILE_NOT_PROVIDE);
        }

        if (!ALLOWED_FILE_EXTENSIONS.contains(file.getContentType())) {
            throw new InputMismatchException(NOT_SUPPORTED);
        }

        Image image = new Image();
        image.setImage(file.getBytes());
        image.setHelpRequest(helpRequest);
        return imageRepository.save(image);
    }

    @Override
    public byte[] downloadImage(Long id) {
        return imageRepository.findById(id).orElseThrow(
                () -> new NoSuchElementException(IMAGE_NOT_FOUND)
        ).getImage();
    }
}
