package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.exception.TokenNotFoundException;
import boot.camp.heartofukrainebootcamp.model.entity.ConfirmationToken;
import boot.camp.heartofukrainebootcamp.model.entity.User;
import boot.camp.heartofukrainebootcamp.repository.ConfirmationTokenRepository;
import boot.camp.heartofukrainebootcamp.service.ConfirmationTokenService;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {
    private final ConfirmationTokenRepository confirmationTokenRepository;

    @Value("${email.confirmation.token.lifetime}")
    private Long tokenLifetime;

    @Override
    public String saveConfirmationToken(ConfirmationToken token) {
        return confirmationTokenRepository.save(token).getToken();
    }

    @Override
    public ConfirmationToken getToken(String token) {
        return confirmationTokenRepository
                .findByToken(token)
                .orElseThrow(() -> new TokenNotFoundException(token));
    }

    @Override
    public ConfirmationToken generateToken(User user) {
        return ConfirmationToken.builder()
                .token(UUID.randomUUID().toString())
                .createdAt(LocalDateTime.now())
                .expiresAt(LocalDateTime.now().plusMinutes(tokenLifetime))
                .user(user)
                .build();
    }
}
