package boot.camp.heartofukrainebootcamp.service.impl;

import boot.camp.heartofukrainebootcamp.dto.region.RegionDto;
import boot.camp.heartofukrainebootcamp.mapper.RegionMapper;
import boot.camp.heartofukrainebootcamp.model.enums.Region;
import boot.camp.heartofukrainebootcamp.service.RegionService;
import java.util.Arrays;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegionServiceImpl implements RegionService {
    private final RegionMapper regionMapper;

    @Override
    public List<RegionDto> findAll() {
        return Arrays.stream(Region.values()).toList()
                .stream()
                .map(regionMapper::toDto)
                .toList();
    }
}
