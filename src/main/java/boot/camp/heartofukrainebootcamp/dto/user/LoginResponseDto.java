package boot.camp.heartofukrainebootcamp.dto.user;

public record LoginResponseDto (String token) {
}
