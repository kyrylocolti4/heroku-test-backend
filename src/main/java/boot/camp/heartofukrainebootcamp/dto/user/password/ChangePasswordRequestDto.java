package boot.camp.heartofukrainebootcamp.dto.user.password;

import boot.camp.heartofukrainebootcamp.validation.field.FieldMatch;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@FieldMatch.List({
        @FieldMatch(first = "newPassword",
                second = "repeatNewPassword",
                message = "Passwords do not match")
})
public class ChangePasswordRequestDto {

    @NotBlank
    @Schema(description = "User Current Password", example = "Qwerty123456")
    private String oldPassword;

    @NotBlank
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", message = "invalid password")
    @Schema(description = "User New Password", example = "Johndoe123")
    private String newPassword;

    @NotBlank
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", message = "invalid password")
    @Schema(description = "User Repeat Password", example = "Johndoe123")
    private String repeatNewPassword;
}
