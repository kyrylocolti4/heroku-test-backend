package boot.camp.heartofukrainebootcamp.dto.user;

import boot.camp.heartofukrainebootcamp.dto.region.RegionDto;
import boot.camp.heartofukrainebootcamp.model.entity.UserAdditionalDetails;
import boot.camp.heartofukrainebootcamp.model.enums.Region;
import lombok.Data;
import org.mapstruct.Mapping;

@Data
public class UserResponseDto {

    private String email;

    private String firstName;

    private String lastName;

    private RegionDto region;

    private UserAdditionalDetails userAdditionalDetails;

    private boolean isEnabled;
}
