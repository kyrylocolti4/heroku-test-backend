package boot.camp.heartofukrainebootcamp.dto.user;

import boot.camp.heartofukrainebootcamp.validation.field.FieldMatch;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@FieldMatch.List({
        @FieldMatch(first = "password",
                second = "repeatPassword",
                message = "Passwords do not match")
})
public class RegistrationRequestDto {

    @Email
    @NotEmpty
    @Schema(description = "User Email", example = "johndoe@gmail.com")
    private String email;

    @NotBlank
    @Size(min = 8, max = 100)
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,100}$", message = "invalid password")
    @Schema(description = "User Password", example = "Qwerty123456")
    private String password;

    @NotBlank
    @Size(min = 8, max = 100)
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,100}$", message = "invalid password")
    @Schema(description = "Repeat Password", example = "Qwerty123456")
    private String repeatPassword;

    @NotBlank
    @Size(min = 2, max = 30)
    @Pattern(regexp = "[A-zА-яҐЄІЇґєії']{2,30}" , message = "first name must contain only letters")
    @Schema(description = "User First Name", example = "John")
    private String firstName;

    @NotBlank
    @Size(min = 2, max = 30)
    @Pattern(regexp = "[A-zА-яҐЄІЇґєії']{2,30}" , message = "last name must contain only letters")
    @Schema(description = "User Last Name", example = "Doe")
    private String lastName;
}
