package boot.camp.heartofukrainebootcamp.dto.user.password;

import boot.camp.heartofukrainebootcamp.validation.field.FieldMatch;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@FieldMatch.List({
        @FieldMatch(first = "newPassword",
                second = "repeatNewPassword",
                message = "Passwords do not match")
})
public class ForgotPasswordRequestDto {
    @NotBlank
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", message = "invalid password")
    @Schema(description = "User New Password", example = "Qwerty12346")
    private String newPassword;

    @NotBlank
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", message = "invalid password")
    @Schema(description = "User New Password", example = "Qwerty12346")
    private String repeatNewPassword;
}
