package boot.camp.heartofukrainebootcamp.dto.helprequest;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CategoryRequestDto {
    @NotBlank
    @Schema(description = "Category english name")
    String categoryNameEn;

    @NotBlank
    @Schema(description = "Category ukrainian name")
    String categoryNameUa;
}
