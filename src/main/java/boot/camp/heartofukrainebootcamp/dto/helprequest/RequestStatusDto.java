package boot.camp.heartofukrainebootcamp.dto.helprequest;

public record RequestStatusDto(String requestStatusEn, String requestStatusUa) {
}
