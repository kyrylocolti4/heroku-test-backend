package boot.camp.heartofukrainebootcamp.dto.helprequest;

public record RequestTypeDto(String requestTypeEn, String requestTypeUa) {
}
