package boot.camp.heartofukrainebootcamp.dto.helprequest;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Set;

@Data
public class HelpRequestRequestDto {
    @NotBlank
    @Schema(description = "Help request title")
    private String title;

    @Size(min = 1, max = 5, message = "Number of categories must be between 1 and 5")
    @Schema(description = "Help request categories")
    private Set<Long> categoryIds;

    @NotBlank
    @Schema(description = "Help request description")
    private String description;

    @NotBlank
    @Schema(description = "Help request region")
    private String region;

    @NotBlank
    @Schema(description = "Help request type")
    private String requestType;

    @Schema(description = "Help request comment")
    private String comment;
}
