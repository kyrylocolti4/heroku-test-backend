package boot.camp.heartofukrainebootcamp.dto.helprequest;

public record CategoryDto(Long id, String categoryNameEn, String categoryNameUa) {
}
