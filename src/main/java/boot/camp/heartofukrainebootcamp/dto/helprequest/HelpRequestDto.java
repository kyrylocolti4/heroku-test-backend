package boot.camp.heartofukrainebootcamp.dto.helprequest;

import boot.camp.heartofukrainebootcamp.dto.region.RegionDto;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import lombok.Builder;

@Builder
public record HelpRequestDto(Long id, String title,
                             Set<CategoryDto> categories, String description,
                             LocalDateTime createdAt, RegionDto region,
                             RequestTypeDto requestType, RequestStatusDto requestStatus,
                             String authorEmail, String executorEmail,
                             String comment, List<Long> images) {
//TODO change authorEmail and executorEmail to UserDto
}
