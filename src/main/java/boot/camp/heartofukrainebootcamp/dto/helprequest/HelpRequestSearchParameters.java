package boot.camp.heartofukrainebootcamp.dto.helprequest;

public record HelpRequestSearchParameters(String[] regions, String[] categories) {
}
