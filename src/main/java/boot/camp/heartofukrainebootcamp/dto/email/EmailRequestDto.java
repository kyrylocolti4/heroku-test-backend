package boot.camp.heartofukrainebootcamp.dto.email;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class EmailRequestDto {

    @Email
    @NotEmpty
    @Schema(description = "User Email", example = "johndoe@gmail.com")
    private String email;
}
