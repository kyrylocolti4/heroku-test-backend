package boot.camp.heartofukrainebootcamp.dto.region;

public record RegionDto(String regionEn, String regionUa) {
}
