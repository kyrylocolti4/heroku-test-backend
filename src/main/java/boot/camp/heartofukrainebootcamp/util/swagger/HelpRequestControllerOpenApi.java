package boot.camp.heartofukrainebootcamp.util.swagger;

import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.HelpRequestRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Tag(name = "HelpRequest Controller", description = "API to work with help request")
public interface HelpRequestControllerOpenApi {
    @Operation(summary = "Create new help request",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Help request successfully created",
                    content = {
                            @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema =
                                    @Schema(implementation = HelpRequestRequestDto.class))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such category is not exists"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such region is not exists"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such request type is not exists")
    })
    HelpRequestDto save(@Valid @RequestBody HelpRequestRequestDto request);

    @Operation(summary = "Get all help requests sorting and page by page",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully fetched help requests"),
            @ApiResponse(
                    responseCode = "500",
                    description = "No property found for type 'HelpRequest'")
    })
    Page<HelpRequestDto> findAll(Pageable pageable);

    @Operation(summary = "Update help request",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Help request successfully updated",
                    content = {
                            @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema =
                                    @Schema(implementation = HelpRequestRequestDto.class))
                    }),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such help request is not exists"),
            @ApiResponse(
                    responseCode = "403",
                    description = "Access forbidden"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such category is not exists"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such region is not exists"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such request type is not exists")
    })
    void update(@PathVariable("id") Long id,
                @Valid @RequestBody HelpRequestRequestDto helpRequestRequest);

    @Operation(summary = "Set executor for help request",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Executor successfully set"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such help request is not exists"),
            @ApiResponse(
                    responseCode = "409",
                    description = "Help request is already at work"),
            @ApiResponse(
                    responseCode = "409",
                    description = "Author can't be executor")
    })
    void setExecutor(@PathVariable("id") Long id);


    @Operation(summary = "Add images to help request",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Images successfully add"),
            @ApiResponse(
                    responseCode = "400",
                    description = "You should provide valid files"),
            @ApiResponse(
                    responseCode = "400",
                    description = "File format is not supported"),
            @ApiResponse(
                    responseCode = "400",
                    description = "You can upload maximum 4 images")
    })
    void addImages(@PathVariable("id") Long id,
                   @Parameter(description = "Files to upload",
                           content = @Content(mediaType = "multipart/form-data",
                                   array = @ArraySchema(schema = @Schema(type = "string", format = "binary"))))
                   @RequestParam("images") MultipartFile[] files) throws IOException;
}
