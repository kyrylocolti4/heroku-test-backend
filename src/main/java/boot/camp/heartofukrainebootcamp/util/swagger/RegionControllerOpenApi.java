package boot.camp.heartofukrainebootcamp.util.swagger;

import boot.camp.heartofukrainebootcamp.dto.region.RegionDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;

@Tag(name = "Region Controller", description = "API to work with region")
public interface RegionControllerOpenApi {
    @Operation(summary = "Get all regions",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully fetched regions")
    })
    List<RegionDto> findAll();
}
