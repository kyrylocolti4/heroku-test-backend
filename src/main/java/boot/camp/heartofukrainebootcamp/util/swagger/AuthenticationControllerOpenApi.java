package boot.camp.heartofukrainebootcamp.util.swagger;

import boot.camp.heartofukrainebootcamp.dto.email.EmailRequestDto;
import boot.camp.heartofukrainebootcamp.dto.user.LoginRequestDto;
import boot.camp.heartofukrainebootcamp.dto.user.LoginResponseDto;
import boot.camp.heartofukrainebootcamp.dto.user.RegistrationRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Tag(name = "Authentication Controller", description = "API to work with authentication")
public interface AuthenticationControllerOpenApi {

    @Operation(summary = "Sign Up")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "User successfully created, confirmation email sent"),
            @ApiResponse(
                    responseCode = "500",
                    description = "Failed to send email message")
    })
    void register(@RequestBody @Valid RegistrationRequestDto request);

    @Operation(summary = "Sign In")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "User successfully authorized",
                    content = {
                            @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema =
                                    @Schema(implementation = LoginRequestDto.class))
                    }),
            @ApiResponse(
                    responseCode = "401",
                    description = "Token has expired")
    })
    LoginResponseDto login(@RequestBody LoginRequestDto request);

    @Operation(summary = "Email Verification")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Email successfully confirmed"),
            @ApiResponse(
                    responseCode = "409",
                    description = "Email is already verified"),
            @ApiResponse(
                    responseCode = "401",
                    description = "Token has expired")
    })
    ResponseEntity<Void> confirm(@RequestParam("token") String token);

    @Operation(summary = "Resend mail message for email verification")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Email sent successfully"),
            @ApiResponse(
                    responseCode = "500",
                    description = "Failed to send email message"),
    })
    void resendConfirmationEmail(@RequestBody EmailRequestDto request);
}
