package boot.camp.heartofukrainebootcamp.util.swagger;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Tag(name = "Image Controller", description = "API to work with images")
public interface ImageControllerOpenApi {

    @Operation(summary = "Upload image to DB",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Image successfully uploaded"),
            @ApiResponse(
                    responseCode = "400",
                    description = "You should provide a valid file"),
            @ApiResponse(
                    responseCode = "400",
                    description = "File format is not supported")
    })
    void uploadImage(@Parameter(description = "File to upload")
                     @Schema(type = "string", format = "binary")
                     @RequestParam("image") MultipartFile file) throws IOException;

    @Operation(summary = "Download image from DB",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Image successfully downloaded"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Image not found")
    })
    ResponseEntity<?> downloadImage(@PathVariable("id") Long id);
}
