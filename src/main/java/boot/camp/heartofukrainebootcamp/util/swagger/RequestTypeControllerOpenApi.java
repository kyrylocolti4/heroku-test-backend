package boot.camp.heartofukrainebootcamp.util.swagger;

import boot.camp.heartofukrainebootcamp.dto.helprequest.RequestTypeDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;

@Tag(name = "Request Type Controller", description = "API to work with request type")
public interface RequestTypeControllerOpenApi {

    @Operation(summary = "Get all request types",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully fetched request types")
    })
    List<RequestTypeDto> findAll();
}
