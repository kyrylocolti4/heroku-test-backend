package boot.camp.heartofukrainebootcamp.util.swagger;

import boot.camp.heartofukrainebootcamp.dto.email.EmailRequestDto;
import boot.camp.heartofukrainebootcamp.dto.user.UserResponseDto;
import boot.camp.heartofukrainebootcamp.dto.user.password.ChangePasswordRequestDto;
import boot.camp.heartofukrainebootcamp.dto.user.password.ForgotPasswordRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Tag(name = "User Controller", description = "API to work with user")
public interface UserControllerOpenApi {

    @Operation(summary = "Change Password",
                security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Password successfully changed"),
            @ApiResponse(
                    responseCode = "400",
                    description = "Invalid old password"),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized")
    })
    void changePassword(@RequestBody ChangePasswordRequestDto request);

    @Operation(summary = "Forgot Password Changing Send Email")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Email sent successfully"),
            @ApiResponse(
                    responseCode = "404",
                    description = "User with specified email not found"),
            @ApiResponse(
                    responseCode = "500",
                    description = "Failed to send email message")
    })
    void forgotPassword(@RequestBody EmailRequestDto request);

    @Operation(summary = "Forgot Password Changing")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Password successfully changed"),
            @ApiResponse(
                    responseCode = "400",
                    description = "Invalid password"),
            @ApiResponse(
                    responseCode = "401",
                    description = "Token has expired")
    })
    void resetPassword(@RequestBody ForgotPasswordRequestDto request,
                       @RequestParam("token") String token);


    @Operation(summary = "Get Current User")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Ok"),
            @ApiResponse(
                    responseCode = "401",
                    description = "Unauthorized")
    })
    UserResponseDto getCurrentUser();
}
