package boot.camp.heartofukrainebootcamp.util.swagger;

import boot.camp.heartofukrainebootcamp.dto.helprequest.CategoryDto;
import boot.camp.heartofukrainebootcamp.dto.helprequest.CategoryRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@Tag(name = "Category Controller", description = "API to work with category")
public interface CategoryControllerOpenApi {

    @Operation(summary = "Create new category (for admins only)",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Category successfully created",
                    content = {
                            @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema =
                                    @Schema(implementation = CategoryRequestDto.class))
                    }),
            @ApiResponse(
                    responseCode = "409",
                    description = "Such category is already exists")
    })
    CategoryDto save(@Valid @RequestBody CategoryRequestDto categoryRequest);

    @Operation(summary = "Get all categories",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully fetched categories")
    })
    List<CategoryDto> findAll();

    @Operation(summary = "Get category by id",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully fetched category"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such category is not exists")
    })
    CategoryDto findById(@PathVariable Long id);

    @Operation(summary = "Update category (for admins only)",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Category successfully updated",
                    content = {
                            @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema =
                                    @Schema(implementation = CategoryRequestDto.class))
                    }),
            @ApiResponse(
                    responseCode = "409",
                    description = "Such category is already exists"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such category is not exists")
    })
    void update(@PathVariable Long id, @Valid @RequestBody CategoryRequestDto categoryRequest);

    @Operation(summary = "Delete category (for admins only)",
            security = @SecurityRequirement(name = "Bearer Authentication"))
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully deleted category by id"),
            @ApiResponse(
                    responseCode = "404",
                    description = "Such category is not exists"),
            @ApiResponse(
                    responseCode = "409",
                    description = "Failed to delete category. " +
                            "There are help requests at this category. At first delete help requests")
    })
    void deleteById(@PathVariable Long id);
}
